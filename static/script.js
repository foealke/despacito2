const puzzles = document.querySelector("#board").children;
const choose = document.querySelector('#choose');

const imgs  = ['full_01_01.png', 'full_01_02.png', 'full_01_03.png', 
               'full_01_04.png', 'full_02_01.png', 'full_02_02.png', 
               'full_02_03.png', 'full_02_04.png', 'full_03_01.png', 
               'full_03_02.png', 'full_03_03.png', 'full_03_04.png'].sort(function() { return 0.5 - Math.random() });

function init(){

    // for(let i=0;i<puzzles.length;i++){
    //     console.dir(puzzles[i]);
    //     puzzles[i].style.backgroundImage = `url("/static/img/1/${ imgs[i] }")`;
    // }
    for(const [i, img]  of imgs.entries() ){
        puzzles[i].style.backgroundImage = `url("/static/img/1/${ img }")`;
    }

}

let startElem;

function onDragStart(evt){
    console.log("dragstart");
    console.log(evt);
    startElem = evt.target;
}

function onDragOver(evt){
    evt.preventDefault();
    console.log("dragover")
}

function onDrop(evt){
    let tmp = evt.target.style.backgroundImage;
    evt.target.style.backgroundImage = startElem.style.backgroundImage;
    startElem.style.backgroundImage = tmp;

    
    console.log('drop');
}

function eventHandler(){
    for(puzzle of puzzles){
        puzzle.addEventListener( "dragstart", onDragStart );  
        puzzle.addEventListener( "dragover", onDragOver );    
        puzzle.addEventListener( "drop", onDrop );    
  
    }
}

init();
eventHandler();


choose.addEventListener('change', (event) => {
    console.log(choose.value);
});


console.log(choose)